import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import image2 from "../image/image 2.png";
import imgDashboard from "../image/dashboard.svg";
import imgDashboardActive from "../image/dashboard-active.svg";
import imgTasks from "../image/tasks.svg";
import imgTasksActive from "../image/tasks-active.svg";
import imgEmail from "../image/email.svg";
import imgEmailActive from "../image/email-active.svg";
import imgContacts from "../image/Contacts.svg";
import imgContactsActive from "../image/contacts-active.svg";
import imgChat from "../image/Chat.svg";
import imgChatActive from "../image/chat-active.svg";
import imgDeals from "../image/Deals.svg";
import imgDealsActive from "../image/deals-active.svg";
import imgSettings from "../image/Settings.svg";
import imgSettingsActive from "../image/settings-active.svg";
import Toggle from "../image/Toggle.svg";
import Yellow from "../image/yellow_circle.svg";
import Green from "../image/green_circle.svg";
import Red from "../image/red_circle.svg";
import Purple from "../image/purple_circle.svg";

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClick: false,
    };
  }
  click(state) {
    this.state({
      isClick: state,
    });
  }
  render() {
    return (
      <header className={this.state.isClick === true ? "hidden" : ""}>
        <nav>
          <NavLink exact className="title" to="/">
            <span>Saas</span>
            <span className="titleHid"> Kit</span>
          </NavLink>
          <NavLink exact className="profile" to="/profile">
            <div className="profileWrap">
              <img src={image2} alt="Картинка профиля" className="imgProfile" />
              <div className="profileWrap2">
                <p className="nameUser">Sierra Ferguson</p>
                <p className="emailUser">s.ferguson@gmail.com</p>
              </div>
            </div>
          </NavLink>
          <NavLink
            exact
            className="navigation dashboard"
            to="/dashboard"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="dashboardActive"
          >
            <img src={imgDashboard} className="dashboard1" />
            <img src={imgDashboardActive} className="dashboard2" />
            <span className="titles">Dashboard</span>
          </NavLink>
          <NavLink
            exact
            className="navigation tasks"
            to="/tasks"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="tasksActive"
          >
            <div className="tasksWrap">
              <img src={imgTasks} className="tasks1" />
              <img src={imgTasksActive} className="tasks2" />
              <span className="titles">Tasks</span>
            </div>
            <ul>
              <li>
                <img src={Yellow} />
                Active
              </li>
              <li>
                <img src={Green} />
                Completed
              </li>
              <li>
                <img src={Red} />
                Ended
              </li>
            </ul>
          </NavLink>
          <NavLink
            exact
            className="navigation email"
            to="/email"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="emailActive"
          >
            <div className="emailWrap">
              <img src={imgEmail} className="email1" />
              <img src={imgEmailActive} className="email2" />
              <span className="titles">Email</span>
            </div>
            <ul>
              <li>
                <img src={Yellow} />
                Draft
              </li>
              <li>
                <img src={Purple} />
                Scheduled
              </li>
              <li>
                <img src={Green} />
                Sent
              </li>
              <li>
                <img src={Red} />
                Archived
              </li>
            </ul>
          </NavLink>
          <NavLink
            exact
            className="navigation contacts"
            to="/contacts"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="contactsActive"
          >
            <img src={imgContacts} className="contacts1" />
            <img src={imgContactsActive} className="contacts2" />
            <span className="titles">Contacts</span>
          </NavLink>
          <NavLink
            exact
            className="navigation chat"
            to="/chat"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="chatActive"
          >
            <img src={imgChat} className="chat1" />
            <img src={imgChatActive} className="chat2" />
            <span className="titles">Chat</span>
          </NavLink>
          <NavLink
            exact
            className="navigation deals"
            to="/deals"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="dealsActive"
          >
            <div className="dealsWrap">
              <img src={imgDeals} className="deals1" />
              <img src={imgDealsActive} className="deals2" />
              <span className="titles">Deals</span>
            </div>
            <ul>
              <li>
                <img src={Yellow} />
                Draft
              </li>
              <li>
                <img src={Purple} />
                Scheduled
              </li>
              <li>
                <img src={Green} />
                Sent
              </li>
              <li>
                <img src={Red} />
                Archived
              </li>
            </ul>
          </NavLink>
          <NavLink
            exact
            className="navigation settings"
            to="/settings"
            activeStyle={{ color: "#109CF1" }}
            activeClassName="settingsActive"
          >
            <img src={imgSettings} className="settings1" />
            <img src={imgSettingsActive} className="settings2" />
            <span className="titles">Settings</span>
          </NavLink>
          <div className="navigation toggle">
            <div
              onClick={() => this.setState({ isClick: !this.state.isClick })}
            >
              <img src={Toggle} />
              <span className="titles">Toggle sidebar</span>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}
