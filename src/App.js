import React from "react";
import "./App.css";
import Menu from "./pages/Menu";
import Main from "./pages/Main";
import Dashboard from "./pages/Dashboard";
import Tasks from "./pages/Tasks";
import Email from "./pages/Email";
import Contacts from "./pages/Contacts";
import Chat from "./pages/Chat";
import Deals from "./pages/Deals";
import Settings from "./pages/Settings";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Profile from "./pages/Profile";

function App() {
  return (
    <div className="App">
      <Router>
        <Menu />
        <Route exact path="/">
          <Main />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
        <Route exact path="/dashboard">
          <Dashboard />
        </Route>
        <Route exact path="/tasks">
          <Tasks />
        </Route>
        <Route exact path="/email">
          <Email />
        </Route>
        <Route exact path="/contacts">
          <Contacts />
        </Route>
        <Route exact path="/chat">
          <Chat />
        </Route>
        <Route exact path="/deals">
          <Deals />
        </Route>
        <Route exact path="/settings">
          <Settings />
        </Route>
      </Router>
    </div>
  );
}

export default App;
